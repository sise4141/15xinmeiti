import Vue from 'vue'
import MintUI from 'mint-ui'
import 'mint-ui/lib/style.css'
import App from './App.vue'
import { Header } from 'mint-ui'
import VueRouter from "vue-router"
import Vuex from 'vuex'

import VueResource from "vue-resource"
import index from './components/index.vue'
import about from './components/about.vue'
import myself from './components/myself.vue'
import product from './components/product.vue'

Vue.config.debug = true;


Vue.use(Vuex)

Vue.use(VueRouter);
Vue.use(MintUI)
Vue.use(VueResource);

const router = new VueRouter({
  mode: 'history',
    base: __dirname,
 routes: [
   {
    path: '/',
    component:index
   },
 {
        path: '/index',
        component:index
 },
 {
        path: '/about',
        component:about,
 },{
        path: '/myself',
        component:myself
 },{
        path: '/product',
        component:product
 }
 
 ]
})

const app = new Vue({
    router: router,
    render: h => h(App)
}).$mount('#app')